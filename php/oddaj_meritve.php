<?php

	session_start();

	if (!(isset($_SESSION['LogedIn']) && $_SESSION['LogedIn'] == true))
	{
		header("Location: php/login.php");
	}
	else
	{
		require_once '../include/config.php';
		$input = json_decode(file_get_contents('php://input'));

		if ($input)
		{
			echo "Obstaja input";
		}

		try
		{
			$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE, DB_USER, DB_PASSWORD);
			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// zagotovi pravilno vpisovanje sumnikov (brez te vrstice se v tabelo pisejo packe namesto sumnikov)
			$conn->exec("set names utf8");

			// nacin z prepared statements
			$sql = $conn->prepare("INSERT INTO sledi (users_fk, levels_fk, time_of_measurement, sled) VALUES ((SELECT unique_id FROM users WHERE username = :username), 1, :cas, :sled)");

			$sql->bindParam(':username', $_SESSION['UserName']);

			// za vsak element asociativnega arraya
			foreach ($input as $x)
			{
				$sql->bindParam(':sled', $x->pravilnost);
				//$sql->bindParam(':stopnja', $x->stopnja);
				$sql->bindValue(':cas', $x->casovniZig);
				$sql->execute();
				echo "New record created successfully - v bazo vpisal eno pravilnost izmed mnogih. xxx CAS = " . $x->casovniZig . " " . $x->pravilnost . "\n";
			}

		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

		$conn = null;

	}

?>
