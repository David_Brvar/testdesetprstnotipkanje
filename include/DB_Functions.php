<?php

class DB_Functions
{
	private $db;
	//put your code here
	// constructor
	function __construct()
	{
		require_once 'DB_Connect.php';
		// connecting to database
		$this->db = new DB_Connect();
		$this->db->connect();
	}

	// destructor
	function __destruct()
	{

	}

	/**
	 * Random string which is sent by mail to reset password
	 */
	public function random_string()
	{
		$character_set_array = array();
		$character_set_array[] = array('count' => 7, 'characters' => 'abcdefghijklmnopqrstuvwxyz');
		$character_set_array[] = array('count' => 1, 'characters' => '0123456789');
		$temp_array = array();

		foreach ($character_set_array as $character_set)
		{
			for ($i = 0; $i < $character_set['count']; $i++)
			{
				$temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
			}
		}
		shuffle($temp_array);
		return implode('', $temp_array);
	}

	/**
	 * login function
	 */
	public function Login($password)
	{
		$_SESSION['sucess']='';

		if(!empty($_SESSION['UserName']) and !empty($password))
		{
			$_SESSION['password-error']="";
			$username=$_SESSION['UserName'];

			$login = $this->getUserByUsernameAndPassword($username,$password);

			if($login)
			{
				$_SESSION['UserName']=$username;
				$_SESSION['sucess']="login succesful!";
				$_SESSION['LogedIn']="true";
				return true;
			}
			else
			{
				$_SESSION['LogedIn']="false";
				return false;
			}
		}
		else
		{
			$cpassword="";
			$_SESSION['LogedIn']="false";
			$i=$this->Unfilled($password,$cpassword);

			if($i)
				{

				$_SESSION['sucess']="You must fill out the form!";
				}
		}
	}

	/**
	 * Verifies if loged in
	 */
	public function IsLogedIn()
	{
		if(!(isset($_SESSION['LogedIn']) && $_SESSION['LogedIn'] != "false"))
		{
			header("Location: login.php");
		}
	}

	/**
	 * logout
	 */
	public function Logout()
	{
		if ($_SESSION['LogedIn']=="true")
		{
		$_SESSION['LogedIn']="false";
		session_destroy();
		header("Location: login.php");
		}
	}

	/**
	 * SignUp
	 */
	public function SignUp($password,$cpassword)
	{
		$_SESSION['sucess']='';

		if(!empty($_SESSION['UserName']) and !empty($_SESSION['Email']) and !empty($_SESSION['FirstName']) and !empty($_SESSION['LastName']) and !empty($password) and !empty($cpassword))
		{
		$_SESSION['cpassword-error']='';
		$_SESSION['FirstName-error']='';
		$_SESSION['LastName-error']='';

		$check=$this->CheckForm($password,$cpassword);


		if($check==0) // save user!
			{
			$newuser=$this->newuser($password);
			}
		}


		else
		{
			$i=$this->Unfilled($password,$cpassword);

			if($i)
				{

				$_SESSION['sucess']="You must fill out the form!";
				return false;
				}
		}
	}
	 /**
	 * NewUser
	 */
	 public function newuser($password)
	{
		$user =$this->storeUser($_SESSION['FirstName'], $_SESSION['LastName'], $_SESSION['Email'], $_SESSION['UserName'], $password);

		if($user)
			{
			$_SESSION['sucess']="Your registration is completed!";


			// odklene prvo stopnjo
			try
			{
				$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE, DB_USER, DB_PASSWORD);
				// set the PDO error mode to exception
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				// nacin z prepared statements
				$sql2 = $conn->prepare("INSERT INTO users_2_levels_2_achievements (users_fk, levels_fk, achievements_fk) VALUES ((SELECT unique_id FROM users WHERE username = :username), :stopnja, :achievement)");

				$input1 = 1;
				$input2 = 1;
				$sql2->bindParam(':stopnja', $input1);
				$sql2->bindParam(':username', $_SESSION['UserName']);
				$sql2->bindParam(':achievement', $input2);
				$sql2->execute();

				echo "Uspesno odklenjena prva stopnja.";

			}
			catch(PDOException $e)
			{
				echo "neuspeh pri odklepanju prve stopnje";
				echo $e->getMessage();
			}

			$conn = null;

			}
		else
			{
			$_SESSION['sucess']="Fail!";
			}


	}
		/**
	 *  preveri forgotpassword!(še ne dela mail)
	 */
	public function forgotPassword()
	{
		require_once 'include/DB_Connect.php';
			$db2 = new DB_Connect();
			$con=$db2->connect();

		$email=$_SESSION['Email'];
		$password=$this->random_string();
		$validEmail=$this->validEmail($email);

		if($validEmail)
		{
			$_SESSION['Email-error']="";

			$isUserExisted=$this->isUserExisted($email);
			if($isUserExisted)
			{
				$hash = $this->hashSSHA($password);
				$encrypted_password = $hash["encrypted"]; // encrypted password
				$salt = $hash["salt"]; // salt
				$result = mysqli_query($con,"UPDATE `users` SET `encrypted_password` = '$encrypted_password',`salt` = '$salt' WHERE `email` = '$email'");
				// check for successful store
				if ($result)
				{
					$to = "$email";
					$subject = "Password reset";
					$txt = "New Password is" . $password ;
					$headers = "From: webmaster@example.com" . "\r\n" .
					"CC: somebodyelse@example.com";

					mail($to,$subject,$txt,$headers);
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$_SESSION['Email-error']="no such email in the database";
				return false;
			}
		}
		else
		{
			$_SESSION['Email-error']="No correct email form.";
			return false;
		}
		mysqli_close($con);
	}
	/**
	 * Adding new user to mysql database
	 * returns user details
	 */
	public function storeUser($fname, $lname, $email, $uname, $password) {
		require_once 'include/DB_Connect.php';
			$db2 = new DB_Connect();
			$con=$db2->connect();

		$uuid = uniqid('', true);//problems isti id
		$hash = $this->hashSSHA($password);
		$encrypted_password = $hash["encrypted"]; // encrypted password
		$salt = $hash["salt"]; // salt
		$result = mysqli_query($con,"INSERT INTO users(unique_id, firstname, lastname, email, username, encrypted_password, salt, created_at) VALUES('$uuid', '$fname', '$lname', '$email', '$uname', '$encrypted_password', '$salt', NOW())");
		// check for successful store
		if ($result) {
			// get user details
			//$uid = mysql_insert_id(); // last inserted id
			//$result = mysql_query("SELECT * FROM users WHERE uid = $uid");
			// return user details
			return true;
		} else {
			return false;
		}
		mysqli_close($con);

	}

	/**
	 * Verifies user by email and password
	 */
	public function getUserByEmailAndPassword($email, $password) {
		require_once 'include/DB_Connect.php';
			$db2 = new DB_Connect();
			$con=$db2->connect();

		$result = mysqli_query($con,"SELECT * FROM users WHERE email = '$email'") or die(mysql_error());/////////////////////////////////?
		// check for result
		$no_of_rows = mysqli_num_rows($result);
		if ($no_of_rows > 0) {
			$result = mysqli_fetch_array($result);
			$salt = $result['salt'];
			$encrypted_password = $result['encrypted_password'];
			$hash = $this->checkhashSSHA($salt, $password);
			// check for password equality
			if ($encrypted_password == $hash) {
				// user authentication details are correct
				return $result;
			}
		} else {
			// user not found
			return false;
		}
		mysqli_close($con);
	}
	 /**
	 * Verifies user by username and password
	 */
	public function getUserByUsernameAndPassword($username, $password)
	{
		require_once 'include/DB_Connect.php';
			$db2 = new DB_Connect();
			$con=$db2->connect();

		$result = mysqli_query($con,"SELECT * FROM users WHERE username = '$username'") or die(mysql_error()); // mysqli_error ??
		// check for result
		$no_of_rows = mysqli_num_rows($result);
		if ($no_of_rows > 0)
		{
			$result = mysqli_fetch_array($result);
			$salt = $result['salt'];
			$encrypted_password = $result['encrypted_password'];
			$hash = $this->checkhashSSHA($salt, $password);
			// check for password equality
			if ($encrypted_password == $hash)
			{
				// user authentication details are correct
				$_SESSION['sucess']="login sucess";
				return true;
			}
			else
			{
			$_SESSION['sucess']="Username/password incorrect";
			return false;
			}
		}
		else
		{
			// user not found
			$_SESSION['sucess']="Username/password incorrect";
			return false;
		}
		//close connection to sql
		mysqli_close($con);
	}

	/**
	 * Checks whether the email is valid or fake
	 */
	public function validEmail($email)
	{
	   $isValid = true;
	   $atIndex = strrpos($email, "@");
	   if (is_bool($atIndex) && !$atIndex)
	   {
		  $isValid = false;
	   }
	   else
	   {
		  $domain = substr($email, $atIndex+1);
		  $local = substr($email, 0, $atIndex);
		  $localLen = strlen($local);
		  $domainLen = strlen($domain);
		  if ($localLen < 1 || $localLen > 64)
		  {
			 // local part length exceeded
			 $isValid = false;
		  }
		  else if ($domainLen < 1 || $domainLen > 255)
		  {
			 // domain part length exceeded
			 $isValid = false;
		  }
		  else if ($local[0] == '.' || $local[$localLen-1] == '.')
		  {
			 // local part starts or ends with '.'
			 $isValid = false;
		  }
		  else if (preg_match('/\.\./', $local))
		  {
			 // local part has two consecutive dots
			 $isValid = false;
		  }
		  else if (!preg_match('/^[A-Za-z0-9\-\.]+$/', $domain))
		  {
			 // character not valid in domain part
			 $isValid = false;
		  }
		  else if (preg_match('/\.\./', $domain))
		  {
			 // domain part has two consecutive dots
			 $isValid = false;
		  }
		  else if
	(!preg_match('/^(\\.|[A-Za-z0-9!#%&`_=\/$*+?^{}|~.-])+$/',
					 str_replace("\\","",$local)))
		  {
			 // character not valid in local part unless
			 // local part is quoted
			 if (!preg_match('/^"(\\"|[^"])+"$/',
				 str_replace("\\","",$local)))
			 {
				$isValid = false;
			 }
		  }
		  if ($isValid && !(checkdnsrr($domain,"MX") ||
	 ↪checkdnsrr($domain,"A")))
		  {
			 // domain not found in DNS
			 $isValid = false;
		  }
	   }
	   return $isValid;
	}

	/**
	 * Check user-email is existed or not
	 */
	public function isUserExisted($email)
		{
		require_once 'include/DB_Connect.php';
			$db1 = new DB_Connect();
			$con=$db1->connect();

		$result = mysqli_query($con,"SELECT email from users WHERE email = '$email'");
		$no_of_rows = mysqli_num_rows($result);
		if ($no_of_rows > 0)
			{
			// user existed
			return true;
			}
		else
			{
			// user not existed
			return false;
			}
		mysqli_close($con);
		}

	/**
	 * Check username is existed or not
	 */
	public function isUsernameTaken($username)
		{
		require_once 'include/DB_Connect.php';
			$db1 = new DB_Connect();
			$con=$db1->connect();

		$result = mysqli_query($con,"SELECT email from users WHERE username = '$username'");
		$no_of_rows = mysqli_num_rows($result);
		if ($no_of_rows > 0)
			{
			// user existed
			return true;
			}
		else
			{
			// user not existed
			return false;
			}
		mysqli_close($con);
		}
	/**
	 * Check what is missing in the form(not working yet!)
	 */
	public function Unfilled($password,$cpassword)
		{

		if(empty($_SESSION['UserName']))
				{
				$_SESSION['Username-error']="<font color='red'>X</font> (missing)";
				}
			else
				{
				$_SESSION['Username-error']="";
				}

			if(empty($_SESSION['Email']))
				{
				$_SESSION['Email-error']="<font color='red'>X</font> (missing)";
				}
			else
				{
				$_SESSION['Email-error']="";
				}

			if(empty($_SESSION['FirstName']))
				{
				$_SESSION['FirstName-error']="<font color='red'>X</font> (missing)";
				}
			else
				{
				$_SESSION['FirstName-error']="";
				}

			if(empty($_SESSION['LastName']))
				{
				$_SESSION['LastName-error']="<font color='red'>X</font> (missing)";
				}
			else
				{
				$_SESSION['LastName-error']="";
				}

			if(empty($password))
				{
				$_SESSION['password-error']="<font color='red'>X</font> (missing)";
				}
			else if (strlen ($password)<6)//long enough password?
				{
				$_SESSION['password-error']="<font color='red'>X</font> (minimal length is 6)";
				}
			else
				{
				$_SESSION['password-error']="";
				}

			if(empty($cpassword))
				{
				$_SESSION['cpassword-error']="<font color='red'>X</font> (missing)";
				}
			else if (strlen ($cpassword)<6)//long enough password?
				{
				$_SESSION['cpassword-error']="<font color='red'>X</font> (minimal length is 6)";
				}
			else
				{
				$_SESSION['cpassword-error']="";
				}



			if(empty($_SESSION['UserName']) and empty($_SESSION['Email']) and empty($_SESSION['FirstName']) and empty($_SESSION['LastName']))
				{
				return true;
				}
		}

	/**
	* Checking
	* the Form
	*/

	public function CheckForm($password,$cpassword)
		{
		require_once 'include/DB_Functions.php';
		$db = new DB_Functions();

		$error=0;

		if ( trim($_SESSION['UserName']) == "" )//username not blank?
			{
			$_SESSION['UserName']='';
			$_SESSION['Username-error']="<font color='red'>X</font> (no blank spaces only)";
			$error=$error+1;//štetje errorjev
			}
		else if($db->isUsernameTaken($_SESSION['UserName'])) //username already taken ?
			{
			$_SESSION['UserName']='';
			$_SESSION['Username-error']="<font color='red'>X</font> (username taken)";
			$error=$error+1;//štetje errorjev
			}
		else
			{
			$_SESSION['Username-error']="";
			}

		if ( trim($_SESSION['FirstName']) == "" )//FirstName not blank?
			{
			$_SESSION['FirstName']='';
			$_SESSION['FirstName-error']="<font color='red'>X</font> (no blank spaces only)";
			$error=$error+1;//štetje errorjev
			}
		else
			{
			$_SESSION['FirstName-error']='';
			}

		if ( trim($_SESSION['LastName']) == "" )//LastName not blank?
			{
			$_SESSION['LastName']='';
			$_SESSION['LastName-error']="<font color='red'>X</font> (no blank spaces only)";
			$error=$error+1;//štetje errorjev
			}
		else
			{
			$_SESSION['LastName-error']='';
			}

		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$",$_SESSION['Email']))// correct form of email?
			{
			$_SESSION['Email']='';
			$_SESSION['Email-error']="<font color='red'>X</font> (not the correct form for an email)";
			$error=$error+1;//štetje errorjev
			}
		else if($db->isUserExisted($_SESSION['Email'])) //email already in use ?
			{
			$_SESSION['Email']='';
			$_SESSION['Email-error']="<font color='red'>X</font> (email in use)";
			$error=$error+1;//štetje errorjev
			}
		else
			{
			$_SESSION['Email-error']="";
			}

		if (strlen ($password)<6)//long enough password?
			{
			$_SESSION['password-error']="<font color='red'>X</font> (minimal length is 6)";
			$error=$error+1;//štetje errorjev
			}
		else if($password !== $cpassword)//password match ?
			{
			$_SESSION['password-error']="<font color='red'>X</font> (passwords dont match)";
			$error=$error+1;//štetje errorjev
			}
		else
			{
			$_SESSION['password-error']="";
			}
		return $error;
		}
	/**
	 * Encrypting password
	 * returns salt and encrypted password
	 */
	public function hashSSHA($password) {
		$salt = sha1(rand());
		$salt = substr($salt, 0, 10);
		$encrypted = base64_encode(sha1($password . $salt, true) . $salt);
		$hash = array("salt" => $salt, "encrypted" => $encrypted);
		return $hash;
	}
	/**
	 * Decrypting password
	 * returns hash string
	 */
	public function checkhashSSHA($salt, $password) {
		$hash = base64_encode(sha1($password . $salt, true) . $salt);
		return $hash;
	}
}
?>
