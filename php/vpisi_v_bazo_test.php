<?php

	session_start();

	if (!(isset($_SESSION['LogedIn']) && $_SESSION['LogedIn'] == true))
	{
	header("Location: php/login.php");
	}
	else
	{
		require_once '../include/config.php';
		$input = json_decode(file_get_contents('php://input'));

		// logira nazaj
		echo "odgovor, uporabnik je " . $_SESSION['UserName'] . "<br />";

		/* logganje vardump v bazo
		ob_start();
		var_dump($user);
		$result = ob_get_clean();*/

		/* PDO nacin */
		try
		{
			$conn = new PDO("mysql:host=".DB_HOST."; dbname=".DB_DATABASE, DB_USER, DB_PASSWORD);
			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// zagotovi pravilno vpisovanje sumnikov (brez te vrstice se v tabelo pisejo packe namesto sumnikov)
			$conn->exec("set names utf8");

			// nacin z prepared statements
			$sql = $conn->prepare("INSERT INTO vpisi (users_fk, sled) VALUES ((SELECT unique_id FROM users WHERE username = :username), :sled)");
			$sql->bindParam(':sled', $input->tipke);
			$sql->bindParam(':username', $_SESSION['UserName']);
			$sql->execute();
			echo "New record created successfully - vsebina vpisanega v bazo je: ".$input->tipke;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

		$conn = null;

	}

?>
