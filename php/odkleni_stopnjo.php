<?php

	session_start();

	if (!(isset($_SESSION['LogedIn']) && $_SESSION['LogedIn'] == true))
	{
		header("Location: php/login.php");
	}
	else
	{
		require_once '../include/config.php';
		$input = json_decode(file_get_contents('php://input'));

		// logira nazaj
		echo "Številka stopnje je: " . $input->trenutnaStopnja . ". Ti si uporabnik " . $_SESSION['UserName'] . ".";
		$input->trenutnaStopnja += 2;

		/* logganje vardump v bazo
		ob_start();
		var_dump($user);
		$result = ob_get_clean();*/

		/* PDO nacin */
		/*
		var posljem = {
			trenutnaStopnja: sharedStates.dobiIgranoStopnjo(),
			rezultatTrenutneStopnje: self.zvezdice,
			odklepNaslednjeStopnje: unlock,
			komentar : "odklepam stopnje like a boss"
		};
		*/

		try
		{
			$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE, DB_USER, DB_PASSWORD);
			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// nacin z prepared statements

			// zagotovi pravilno vpisovanje sumnikov (brez te vrstice se v tabelo pisejo packe namesto sumnikov)
			$conn->exec("set names utf8");


			// odkleni naslednjo stopnjo, ce se ni
			if ($input->odklepNaslednjeStopnje)
			{
				try
				{
					$sql1 = $conn->prepare("INSERT INTO users_2_levels_2_achievements (users_fk, levels_fk, achievements_fk) VALUES ((SELECT unique_id FROM users WHERE username = :username), :stopnja, 1)");
					$sql1->bindParam(':stopnja', $input->trenutnaStopnja);
					$sql1->bindParam(':username', $_SESSION['UserName']);
					$sql1->execute();
					echo "\nNew record created successfully for new level.";
				}
				catch(PDOException $e)
				{
					echo $e->getMessage();
				}
			}


			$sql2 = $conn->prepare("INSERT INTO users_2_levels_2_achievements (users_fk, levels_fk, achievements_fk) VALUES ((SELECT unique_id FROM users WHERE username = :username), :stopnja, :achievement)");

			$input->trenutnaStopnja -= 1;
			$sql2->bindParam(':stopnja', $input->trenutnaStopnja);
			$sql2->bindParam(':username', $_SESSION['UserName']);
			$sql2->bindParam(':achievement', $input->rezultatTrenutneStopnje);
			$sql2->execute();
			echo "\nNew record created successfully for old level.";

		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

		$conn = null;

	}

?>
