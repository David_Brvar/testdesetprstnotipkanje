(function() {
	var app = angular.module("mojCanvasStuff", []);
	app.factory("canstuff", ["sharedStates", "barvniProstori", "postavitevRok", function(sharedStates, barvniProstori, postavitevRok) {

        // ------------------------------------------------------------------------------------

        var self = this,
        	fps = 60;

        // ------------------------------------------------------------------------------------

        function inicializirajCanvas() {

        	console.log("Inicializiram osrednji canvas.");

            // zacasni canvas za okvir
            tempCanvas1 = document.createElement("canvas");
            tempCtx1 = tempCanvas1.getContext("2d");
            tempOkvir = false;

            // zacasni canvas za crke
            tempCanvas2 = document.createElement("canvas");
            tempCtx2 = tempCanvas2.getContext("2d");
            tempCrke = false;

            // realni osrednji canvas
            canvas = document.getElementById("typingCanvas");
            ctx = canvas.getContext("2d");

            prostorCrke = 200;

            canvas.width = window.innerWidth - 20; // varnostna meja 20px, da brskalnik ne prikaze horizontalnega scroll bara
            canvas.height = 350; // fiksna visina

            tempCanvas2.width = canvas.width + prostorCrke;
            tempCanvas2.height = canvas.height;

            tempCtx2.font = "normal 28em Lucida Console";
            ctx.font = "normal 20em Lucida Console";

            canvasMere = {
                x: (canvas.width - prostorCrke)/2, // zacetna pozicija crk po x, zacne tocno na polovici zaslona
                y: canvas.height - 75 // zacetna pozicija crk po y, rocno doloceno da zacne priblizno na polovici canvasa
            };

            stevecAnimacije = 0;
            self.dbText = sharedStates.getExerciseText();

            indeksBarve = 0;

            ozadjaRGB = new Array(6);
            ozadjaRGB[0] = new rgbOzadje(252, 238, 189); // #fceebd
            ozadjaRGB[1] = new rgbOzadje(252, 238, 132); // #fcee84
            ozadjaRGB[2] = new rgbOzadje(252, 238, 33); // #fcee21
            ozadjaRGB[3] = new rgbOzadje(140, 198, 63); // #8cc63f
            ozadjaRGB[4] = new rgbOzadje(197, 234, 151); // #c5ea97
            ozadjaRGB[5] = new rgbOzadje(237, 238, 198); // #edeec6

            ozadjaHSL = new Array(6);

            for (var i = 0; i < ozadjaHSL.length; i++) {
            	var x = barvniProstori.rgbToHsl(ozadjaRGB[i].r, ozadjaRGB[i].g, ozadjaRGB[i].b);
            	ozadjaHSL[i] = new hslOzadje(x[0], x[1], x[2]);
            }

            indeksTrenutnegaOzadja = 0;
            trenutnoOzadjeHSL = JSON.parse(JSON.stringify(ozadjaHSL[indeksTrenutnegaOzadja]));

            spremeniBarvoOzadja("zacetni");
        }

        // ------------------------------------------------------------------------------------

        function rgbOzadje(r, g, b) {
        	this.r = r;
        	this.g = g;
        	this.b = b;
        }

        // ------------------------------------------------------------------------------------

        function hslOzadje(h, s, l) {
        	this.h = h;
        	this.s = s;
        	this.l = l;
        }

        // ------------------------------------------------------------------------------------

        window.addEventListener("resize", function(event) {
        	risiCanvas();
        }, false);

        // ------------------------------------------------------------------------------------

        // za animacijo canvasa
        window.requestAnimFrame = (function() {
        	return window.requestAnimationFrame ||
        	window.webkitRequestAnimationFrame ||
        	window.mozRequestAnimationFrame ||
        	window.oRequestAnimationFrame ||
        	window.msRequestAnimationFrame;
        })();

        var lastAnimationTime = new Date().getTime();

        function pokaziFPS() {
        	ctx.fillStyle = "Black";
        	ctx.font = "normal 2em Arial";
        	fps = Math.round(fps);
        	ctx.fillText(fps + " fps", 50, 20);

            // postavi nazaj default font
            ctx.font = "normal 20em Lucida Console";
        }

        // ------------------------------------------------------------------------------------

        function spremeniBarvoOzadja(x) {

        	if (x === "posvetli") {

        		console.log("Sem v funkciji spremeniBarvoOzadja z argumentom 'posvetli' .");

        		if (JSON.stringify(ozadjaHSL[indeksTrenutnegaOzadja]) === JSON.stringify(trenutnoOzadjeHSL)) {

        			//console.log("Sedaj pa sta ozadja enaka!", ozadjaHSL[indeksTrenutnegaOzadja], trenutnoOzadjeHSL);

        			(indeksTrenutnegaOzadja === 5) ? indeksTrenutnegaOzadja = 0 : indeksTrenutnegaOzadja++;
        			trenutnoOzadjeHSL = JSON.parse(JSON.stringify(ozadjaHSL[indeksTrenutnegaOzadja]));

        		} else {

                    // povecaj L
                    //console.log("posvetljujem l");
                    trenutnoOzadjeHSL.l += 0.1;

                }

            } else if (x === "potemni") {

            	console.log("Sem v funkciji spremeniBarvoOzadja z argumentom 'potemni' .");

                // pomanjsaj L
                if (trenutnoOzadjeHSL.l > 0.1) {
                	//console.log("potemnjujem l");
                	trenutnoOzadjeHSL.l -= 0.1;
                }

            }

            // THISSSSSSSSSSSSSSSSSSSSSSSS
            // document.body.style.background = "url(img/tekstura.jpg) no-repeat center center fixed, linear-gradient(to right, hsla(190, 50%, 45%, 1), white, hsla(170, 50%, 45%, 1))";

            var barvaTemp = barvniProstori.hslToRgb(trenutnoOzadjeHSL.h, trenutnoOzadjeHSL.s, trenutnoOzadjeHSL.l);
            //document.body.style.background = "linear-gradient(to right, rgb(" + barvaTemp[0] + ", " + barvaTemp[1] + ", " + barvaTemp[2] + "), white, rgb(" + barvaTemp[0] + ", " + barvaTemp[1] + ", " + barvaTemp[2] + "))";

            /* document.body.style.background = "url(img/tekstura.jpg) no-repeat center center fixed, linear-gradient(to right, rgb(" + barvaTemp[0] + ", " + barvaTemp[1] + ", " + barvaTemp[2] + "), white, rgb(" + barvaTemp[0] + ", " + barvaTemp[1] + ", " + barvaTemp[2] + "))"; */


        }

        function risiCanvas() {
            canvas.width  = window.innerWidth - 20; // varnostna meja 20px, da brskalnik ne prikaze horizontalnega scroll bara
            canvasMere.x = (canvas.width - prostorCrke)/2;

            tempCanvas2.width = window.innerWidth - 20 + prostorCrke;

            // font se drugace ob resizu iz neznanega razloga resetira, zato ga je treba se enkrat postaviti na default
            ctx.font = "normal 20em Lucida Console";
            tempCtx2.font = "normal 29em Lucida Console";
            risiCrke();
        }

        // izdela okvir okoli crke
        function risiOkvir(razmerje) {

        	var dolzina_x = 200,
        	dolzina_y = 215,
        	radij = 25,
        	debelinaCrte = 10;

        	if (!tempOkvir) {

        		tempCanvas1.width = dolzina_x + 2 * radij + debelinaCrte;
        		tempCanvas1.height = dolzina_y + 2 * radij + debelinaCrte;

        		//console.log("Postavljam temp canvas za okvir.");
        		tempCtx1.beginPath();

        		var napacne = sharedStates.getUserMistakes();
        		if (napacne[napacne.length-1] === 1) {

                    // rdec okvir
                    tempCtx1.strokeStyle = "rgb(249, 20, 26)";


                } else {

                    // zelen okvir
                    tempCtx1.strokeStyle = "rgb(11, 150, 69)";
                }


                tempCtx1.lineWidth = debelinaCrte;

                // horizontalno okvir vedno povsem centriran
                var x = radij + debelinaCrte/2 + dolzina_x/2;
                var y = debelinaCrte/2;
                tempCtx1.moveTo(x, y);

                tempCtx1.lineTo(x + dolzina_x/2, y);
                tempCtx1.arc(x + dolzina_x/2, y + radij, radij, 1.5 * Math.PI, 2 * Math.PI);

                tempCtx1.lineTo(x + dolzina_x/2 + radij, y + radij + dolzina_y);
                tempCtx1.arc(x + dolzina_x/2, y + radij + dolzina_y, radij, 0 * Math.PI, 0.5 * Math.PI);

                tempCtx1.lineTo(x - dolzina_x/2, y + 2 * radij + dolzina_y);
                tempCtx1.arc(x - dolzina_x/2, y + radij + dolzina_y, radij, 0.5 * Math.PI, 1 * Math.PI);

                tempCtx1.lineTo(x - dolzina_x/2 - radij, y + radij);
                tempCtx1.arc(x - dolzina_x/2, y + radij, radij, 1 * Math.PI, 1.5 * Math.PI);

                tempCtx1.lineTo(x, y);

                tempCtx1.stroke();

                var dolzinaTotal = 2 * Math.PI * radij + dolzina_x * 2 + dolzina_y * 2,
                dolzinaProgress = dolzinaTotal * razmerje,
                krozniLok = radij * Math.PI/2,
                delcki = [dolzina_x/2, krozniLok, dolzina_y, krozniLok, dolzina_x, krozniLok, dolzina_y, krozniLok, dolzina_x/2];

                tempCtx1.beginPath();
                tempCtx1.strokeStyle = "white";
                tempCtx1.lineWidth = 5;

                tempCtx1.moveTo(x, y);

                napredekBar:
                for (var i = 0; i <= 8; i++) {
                	switch(i) {
                		case 0:
                		//console.log("v kejsu 0, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[0]) {
                			tempCtx1.lineTo(x + dolzina_x/2, y);
                		} else {
                			tempCtx1.lineTo(x + dolzinaProgress, y);
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[0];
                		break;
                		case 1:
                		//console.log("v kejsu 1, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[1]) {
                			tempCtx1.arc(x + dolzina_x/2, y + radij, radij, 1.5 * Math.PI, 2 * Math.PI);
                		} else {
                			//console.log("v prvem arcu");
                			tempCtx1.arc(x + dolzina_x/2, y + radij, radij, 1.5 * Math.PI, Math.PI * (1.5 + 0.5 * (dolzinaProgress/delcki[1])));
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[1];
                		break;
                		case 2:
                		//console.log("v kejsu 2, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[2]) {
                			tempCtx1.lineTo(x + dolzina_x/2 + radij, y + radij + dolzina_y);
                		} else {
                			tempCtx1.lineTo(x + dolzina_x/2 + radij, y + radij + dolzinaProgress);
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[2];
                		break;
                		case 3:
                		//console.log("v kejsu 3, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[3]) {
                			tempCtx1.arc(x + dolzina_x/2, y + radij + dolzina_y, radij, 0 * Math.PI, 0.5 * Math.PI);
                		} else {
                			tempCtx1.arc(x + dolzina_x/2, y + radij + dolzina_y, radij, 0 * Math.PI, Math.PI * (0 + 0.5 * (dolzinaProgress/delcki[3])));
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[3];
                		break;
                		case 4:
                		//console.log("v kejsu 4, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[4]) {
                			tempCtx1.lineTo(x - dolzina_x/2, y + 2 * radij + dolzina_y);
                		} else {
                			tempCtx1.lineTo(x + dolzina_x/2 - dolzinaProgress, y + 2 * radij + dolzina_y);
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[4];
                		break;
                		case 5:
                		//console.log("v kejsu 5, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[5]) {
                			tempCtx1.arc(x - dolzina_x/2, y + radij + dolzina_y, radij, 0.5 * Math.PI, 1 * Math.PI);
                		} else {
                			tempCtx1.arc(x - dolzina_x/2, y + radij + dolzina_y, radij, 0.5 * Math.PI, Math.PI * (0.5 + 0.5 * (dolzinaProgress/delcki[5])));
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[5];
                		break;
                		case 6:
                		//console.log("v kejsu 6, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[6]) {
                			tempCtx1.lineTo(x - dolzina_x/2 - radij, y + radij);
                		} else {
                			tempCtx1.lineTo(x - dolzina_x/2 - radij, y + radij + dolzina_y - dolzinaProgress);
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[6];
                		break;
                		case 7:
                		//console.log("v kejsu 7, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[7]) {
                			tempCtx1.arc(x - dolzina_x/2, y + radij, radij, 1 * Math.PI, 1.5 * Math.PI);
                		} else {
                			tempCtx1.arc(x - dolzina_x/2, y + radij, radij, 1 * Math.PI, Math.PI * (1 + 0.5 * (dolzinaProgress/delcki[7])));
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[7];
                		break;
                		case 8:
                		//console.log("v kejsu 8, dolzinaProgress je", dolzinaProgress);
                		if(dolzinaProgress >= delcki[8]) {
                			tempCtx1.lineTo(x, y);
                		} else {
                			tempCtx1.lineTo(x - (dolzina_x/2 - dolzinaProgress), y);
                			break napredekBar;
                		}
                		dolzinaProgress -= delcki[8];
                		break;
                		default:
                		break;
                	}
                }
                tempCtx1.stroke();
            }

            //console.log("Risem iz temp canvasa za okvir v pravi canvas.");
            ctx.drawImage(tempCanvas1, canvasMere.x - radij, canvasMere.y - dolzina_y - radij);
            tempOkvir = true;
        }


        function zelenGradient() {
        	var gradient = tempCtx2.createLinearGradient(0, 0, canvas.width, 0);

        	gradient.addColorStop("0", "rgba(135, 196, 63, 1)");
        	gradient.addColorStop("0.5", "rgba(11, 150, 69, 1)");
        	tempCtx2.fillStyle = gradient;


        }

        function rdecGradient() {
        	var gradient = tempCtx2.createLinearGradient(0, 0, canvas.width, 0);
        	gradient.addColorStop("0", "rgb(252, 139, 142)");
        	gradient.addColorStop("0.5", "rgb(249, 20, 26)");
        	tempCtx2.fillStyle = gradient;

        	tempCtx2.shadowColor = "black";
        	tempCtx2.shadowOffsetX = 2;
        	tempCtx2.shadowOffsetY = 2;
        }

        function svetlaSiva() {
        	tempCtx2.fillStyle = "rgb(204, 204, 204)";

        	tempCtx2.shadowColor = "black";
        	tempCtx2.shadowOffsetX = 2;
        	tempCtx2.shadowOffsetY = 2;
        }

        function temnaSiva() {
        	tempCtx2.fillStyle = "grey";
        	tempCtx2.shadowColor = "black";
        	tempCtx2.shadowOffsetX = 2;
        	tempCtx2.shadowOffsetY = 2;
        }


        function risiCrke() {

        	requestID = requestAnimFrame(risiCrke);

            // v 5 korakih izdela animacijo (neodvisno od framerata)
            if (stevecAnimacije <= 0.9) {

            	stevecAnimacije += 0.2;
                // brisi povrsino
                ctx.clearRect(0, 0, canvas.width, canvas.height);

                // chrome vs firefox font width, uporabljamo monospaced font
                var velikostCrke = ctx.measureText(" ").width;
                //console.log("velikost teksta v pravem je bila", velikostCrke, "velikost v imaginarnem pa", tempCtx2.measureText(" ").width);

                var delta = (new Date().getTime() - lastAnimationTime)/1000;
                lastAnimationTime = new Date().getTime();
                fps = 1 / delta;

                if(!tempCrke) {

                	tempCtx2.clearRect(0, 0, tempCanvas2.width, tempCanvas2.height);
                	//console.log("Postavljam temp canvas za crke.");

                	var userText = sharedStates.getUserText();
                	var napacne = sharedStates.getUserMistakes();

                	var indeks = userText.length-1;
                    // indeks je -1 na zacetku

                    var zapis = self.dbText.concat();
                    napacne = napacne.concat();

                    // -------------------

                    var procent = (userText.length/zapis.length)*100;

                    // -------------------

                    // za string variabilne dolzine spreminjaj spremenljivko koliko
                    var koliko = 14;

                    if (indeks >= koliko/2) {
                    	zapis = zapis.substring(indeks-koliko/2, indeks+koliko/2);
                    	napacne = napacne.splice(indeks-koliko/2, koliko);
                    	indeks = koliko/2;
                    } else {
                    	zapis = zapis.substring(0, koliko);
                    }

                    // izrisi oris pravilnega teksta

                    //ctx.strokeText(zapis, canvasMere.x - indeks*prostorCrke - prostorCrke*stevecAnimacije, canvasMere.y);

                    /*for (var i = 0; i < zapis.length; i++) {
                        //ctx.strokeText(zapis[i], canvasMere.x + (i-indeks)*prostorCrke + (prostorCrke - velikostCrke)/2 - prostorCrke*stevecAnimacije, canvasMere.y);
                        tempCtx2.strokeText(zapis[i], canvasMere.x + (i-indeks)*prostorCrke + (prostorCrke - velikostCrke)/2 - prostorCrke*stevecAnimacije, canvasMere.y);
                    }*/

                    // izrisi userjev tekst
                    for (var i = 0; i < zapis.length; i++) {
                    	if (napacne[i] === 1) {
                    		rdecGradient();
                    	} else if (napacne[i] === 0) {
                    		zelenGradient();
                    	} else {
                    		svetlaSiva();
                    	}

                    	if (i === indeks+1) {
                    		temnaSiva();
                    	}
                        //ctx.fillText(zapis[i], canvasMere.x + (i-indeks)*prostorCrke + (prostorCrke - velikostCrke)/2 - prostorCrke*stevecAnimacije, canvasMere.y);
                        tempCtx2.fillText(zapis[i], canvasMere.x + (i-indeks)*prostorCrke + (prostorCrke - velikostCrke)/2 - prostorCrke*stevecAnimacije, canvasMere.y);
                    }
                    postavitevRok.prikaziRoko(self.dbText[userText.length]);
                    tempCrke = true;
                }

                //console.log("Risem iz temp canvasa za crke v pravi canvas.");
                // 40 px meje ob robovih!
                ctx.drawImage(tempCanvas2, 40 - prostorCrke*stevecAnimacije, 0);

                risiOkvir(procent/100);
                updateProgress(procent);
                //pokaziFPS();
            }
            else {
            	tempOkvir = false;
            	tempCrke = false;
            	stevecAnimacije = 0;
            	//console.log("Konec animacije", requestID);
            	cancelAnimationFrame(requestID);
            }

        }

        function prekiniPrejsnjeAnimacije() {
        	stevecAnimacije = 0;
        	//console.log("Konec animacije prek namenske funkcije", requestID);
        	cancelAnimationFrame(requestID);
        }

        return {
        	risiCrke: risiCrke,
        	spremeniBarvoOzadja: spremeniBarvoOzadja,
        	inicializirajCanvas: inicializirajCanvas,
        	prekiniPrejsnjeAnimacije: prekiniPrejsnjeAnimacije
        };

    }]);

app.factory("barvniProstori", function() {
        // ----------------------------------------------------------- BARVE -------------------------

        /**
         * Converts an HSL color value to RGB. Conversion formula
         * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
         * Assumes h, s, and l are contained in the set [0, 1] and
         * returns r, g, and b in the set [0, 255].
         *
         * @param   Number  h       The hue
         * @param   Number  s       The saturation
         * @param   Number  l       The lightness
         * @return  Array           The RGB representation
         */
         function hslToRgb(h, s, l){
         	var r, g, b;

         	if(s == 0){
                r = g = b = l; // achromatic
            }else{
            	var hue2rgb = function hue2rgb(p, q, t){
            		if(t < 0) t += 1;
            		if(t > 1) t -= 1;
            		if(t < 1/6) return p + (q - p) * 6 * t;
            		if(t < 1/2) return q;
            		if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            		return p;
            	}

            	var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            	var p = 2 * l - q;
            	r = hue2rgb(p, q, h + 1/3);
            	g = hue2rgb(p, q, h);
            	b = hue2rgb(p, q, h - 1/3);
            }

            return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
        }

        /**
         * Converts an RGB color value to HSL. Conversion formula
         * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
         * Assumes r, g, and b are contained in the set [0, 255] and
         * returns h, s, and l in the set [0, 1].
         *
         * @param   Number  r       The red color value
         * @param   Number  g       The green color value
         * @param   Number  b       The blue color value
         * @return  Array           The HSL representation
         */
         function rgbToHsl(r, g, b){
         	r /= 255, g /= 255, b /= 255;
         	var max = Math.max(r, g, b), min = Math.min(r, g, b);
         	var h, s, l = (max + min) / 2;

         	if(max == min){
                h = s = 0; // achromatic
            }else{
            	var d = max - min;
            	s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            	switch(max){
            		case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            		case g: h = (b - r) / d + 2; break;
            		case b: h = (r - g) / d + 4; break;
            	}
            	h /= 6;
            }

            return [h, s, l];
        }

        // ----------------------------------------------------------- BARVE -------------------------

        return {
        	hslToRgb: hslToRgb,
        	rgbToHsl: rgbToHsl
        };
    });

})();
