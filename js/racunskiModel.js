(function() {

    // performanse javascripta => http://jsperf.com/last-array-element2

    var app = angular.module("mojRacunskiModel", []);
    app.factory("racunski", ["audioPlayback", "$http", function(audioPlayback, $http) {

        /* vzorcev za regularno meritev */
        var potrebnoVzorcev = 4;

    	/* utezi */
    	var BETA_0 = 1,
    		BETA_1 = 0.01,
    		BETA_2 = 500,
    		BETA_3 = 800;

    	var trenutnePravilnosti = [],
    		pravilnosti = [],
    		pozornosti = [],
    		utrujenosti = [];


    	/* neposredni stiki tipk za napako tipa 1 */
    	var crkeSosede = {
    		"q" : ["w", "a"],
    		"w" : ["q", "e", "a", "s"],
    		"e" : ["w", "r", "s", "d"],
    		"r" : ["e", "t", "d", "f"],
    		"t" : ["r", "z", "f", "g"],
    		"z" : ["t", "u", "g", "h"],
    		"u" : ["z", "i", "h", "j"],
    		"i" : ["u", "o", "j", "k"],
    		"o" : ["i", "p", "k", "l"],
    		"p" : ["o", "š", "l", "č"],
    		"š" : ["p", "đ", "č", "ć"],
    		"đ" : ["š", "ć", "ž"],

    		"a" : ["q", "w", "s", "<", "y"],
    		"s" : ["w", "e", "a", "s", "y", "x"],
    		"d" : ["e", "r", "s", "f", "x", "c"],
    		"f" : ["r", "t", "d", "g", "c", "v"],
    		"g" : ["t", "z", "f", "h", "v", "b"],
    		"h" : ["z", "u", "g", "j", "b", "n"],
    		"j" : ["u", "i", "h", "k", "n", "m"],
    		"k" : ["i", "o", "j", "l", "m", ","],
    		"l" : ["o", "p", "k", "č", ",", "."],
    		"č" : ["p", "š", "l", "ć", ".", "-"],
    		"ć" : ["š", "đ", "č", "ž", "-"],
    		"ž" : ["đ", "ć"], // shift ne stejemo!

    		"y" : ["a", "s", "<", "x"],
    		"x" : ["s", "d", "y", "c"],
    		"c" : ["d", "f", "x", "v"],
    		"v" : ["f", "g", "c", "b"],
    		"b" : ["g", "h", "v", "n"],
    		"n" : ["h", "j", "b", "m"],
    		"m" : ["j", "k", "n", ","],
    		"," : ["k", "l", "m", "."],
    		"." : ["l", "č", ",", "-"],
    		"-" : ["č", "ć", "."] // nekateri layouti tipkovnic desno od - se nek cuden znak pred shifom
    	};

        // ce ni tipa 1 je avtomatsko tipa 2 :)
        function preveriTipNapake(a, b) {
            if (crkeSosede.hasOwnProperty(a)) {
                for (var i = 0; i < crkeSosede[a].length; i++) {
                    if (crkeSosede[a][i] === b) {
                        return true;
                    }
                }
            }
        }


    	function racunajPravilnost(zakasnitev, pravilnaCrka, uporabnikovaCrka) {

            var napakaTip1 = 0,
                napakaTip2 = 0;

            if (pravilnaCrka !== uporabnikovaCrka) {
                preveriTipNapake(pravilnaCrka, uporabnikovaCrka) ? napakaTip1 = 1 : napakaTip2 = 1;
                console.log("Tip napake1 je ", napakaTip1, " in tip napake2 je ", napakaTip2);
            }

    		var zacasna = BETA_0 + BETA_1 * zakasnitev + BETA_2 * napakaTip1 +  BETA_3 * napakaTip2;

    		trenutnePravilnosti.push(zacasna);
            pravilnosti.push({
    			casovniZig: new Date().getTime(),
    			pravilnost: zacasna
    		});

    		// se upostevajo pri meritvi
            console.log(trenutnePravilnosti);

            // zgodovina vseh
            console.table(pravilnosti);
    	}

    	function racunaj_pozornost_in_utrujenost() {
    		if (trenutnePravilnosti.length > potrebnoVzorcev) {

                // ce se vmes doda crka pride do napake! POPRAVITI !!!------------------

                //console.log("Vzorec vecji od potrebnoVzorcev.");
    			var povprecje = racunajPovprecje(trenutnePravilnosti);
                var varianca = racunajVarianco(trenutnePravilnosti, povprecje);

                //var indeks = pravilnosti.length - trenutnePravilnosti.length;
                //var dolzina = trenutnePravilnosti.length;

    			pozornosti.push({
    				casovniZig: new Date().getTime(),
    				pozornost: povprecje
                    //indeks: indeks,
                    //korak: dolzina
    			});
                utrujenosti.push({
                    casovniZig: new Date().getTime(),
                    utrujenost: varianca
                    //indeks: indeks,
                    //korak: dolzina
                });

                trenutnePravilnosti = [];

                // posreduj zadnjo pozornost za risanje smejkota -------------------
                smejko(pozornosti[pozornosti.length-1]);
                audioPlayback.playSound("sounds/correctWord.wav", 0.5);

    		} else {
                //console.log("Vzorec manjsi od potrebnoVzorcev.");
    			trenutnePravilnosti = [];
    		}
    	}

    	// vhod je matrika stevil
    	function racunajPovprecje(vzorec) {
    		var velikostVzorca = vzorec.length,
    			sestevek = 0,
    			povprecje = 0;

    		for (var i = 0; i < velikostVzorca; i++) {
    			sestevek += vzorec[i];
    		}

    		povprecje = sestevek / velikostVzorca;
    		return povprecje;
    	}

    	// vhod je matrika stevil
    	function racunajVarianco(vzorec, povprecje) {
    		var velikostVzorca = vzorec.length,
    			deviacije = 0,
    			varianca = 0;

    		for (var i = 0; i < velikostVzorca; i++) {
    			deviacije += (vzorec[i] - povprecje)^2;
    		}

    		varianca = Math.abs(deviacije / (velikostVzorca - 1)); // vzorec vedno vsaj 4, ni deljenja z 0!!!!
    		return varianca;
    	}

        function dobiPravilnosti() {
            return pravilnosti;
        }
        function dobiPozornosti() {
            return pozornosti;
        }
        function dobiUtrujenosti() {
            return utrujenosti;
        }

        function prazniMeritve() {
            console.log("Praznim vse meritve.");

            trenutnePravilnosti = [],
            pravilnosti = [],
            pozornosti = [],
            utrujenosti = [];
        }

        function oddajMeritve() {
        	console.log("Oddajam meritve mysqlu/php", pravilnosti);
        	$http({method: "POST", url: "php/oddaj_meritve.php", headers: {"Content-Type": "application/json"}, data: pravilnosti});
        }

        return {
            racunajPravilnost: racunajPravilnost,
            racunaj_pozornost_in_utrujenost: racunaj_pozornost_in_utrujenost,
            dobiPravilnosti: dobiPravilnosti,
            dobiPozornosti: dobiPozornosti,
            dobiUtrujenosti: dobiUtrujenosti,
            prazniMeritve: prazniMeritve,
            oddajMeritve: oddajMeritve
        };

    }]);

    app.factory("postavitevRok", function() {

        var imenaPrstov = ["levi_mezinec", "levi_prstanec", "levi_sredinec", "levi_kazalec", "levi_palec", "desni_palec", "desni_kazalec", "desni_sredinec", "desni_prstanec", "desni_mezinec"];

        var prstiCrk = {
            "q" : 1,
            "w" : 2,
            "e" : 3,
            "r" : 4,
            "t" : 4,
            "z" : 7,
            "u" : 7,
            "i" : 8,
            "o" : 9,
            "p" : 10,
            "š" : 10,
            "đ" : 10,

            "a" : 1,
            "s" : 2,
            "d" : 3,
            "f" : 4,
            "g" : 4,
            "h" : 7,
            "j" : 7,
            "k" : 8,
            "l" : 9,
            "č" : 10,
            "ć" : 10,
            "ž" : 10,

            "y" : 1,
            "x" : 2,
            "c" : 3,
            "v" : 4,
            "b" : 4,
            "n" : 7,
            "m" : 8,
            "," : 9,
            "." : 10,
            "-" : 10,
            // space za palca
            " " : 5,
            "," : 8,
            "." : 9,
            "-" : 10,
            "\"" : 2
        };

        function prikaziRoko(crka) {
            if (crka) {
                var crka = crka.toLowerCase();
            }
            if (prstiCrk[crka]) {
                document.getElementById("tipkarskeRoke").src = "img/" + imenaPrstov[prstiCrk[crka]-1] + ".svg";
            }
        }

        return {
            prikaziRoko: prikaziRoko
        };
    });


    app.factory("time", ["racunski", function(racunski) {

        // http://codepen.io/lavoiesl/pen/GdqDJ

        /* casi merjenja */
        var t1 = 4000,
            t2 = 1500;

        function setCorrectingInterval(func, delay) {

            if (!(this instanceof setCorrectingInterval)) {
                return new setCorrectingInterval(func, delay);
            }

            var target = (new Date().valueOf()) + delay;
            var that = this;

            function tick() {
                if (that.stopped) {
                    return;
                }

                target += delay;
                func();

                setTimeout(tick, target - (new Date().valueOf()));
            }

            setTimeout(tick, delay);
        }

        function clearCorrectingInterval(interval) {
            interval.stopped = true;
        }

        function zacniCasovnik() {
            var startTime = Date.now();
            spremenljivkaCasovnik = setCorrectingInterval(function() {
                //console.log("Selfcorrecting timeout prozen", Date.now() - startTime, "ms je preteklo.");
                //console.log("Racunam pozornost in utrujenost. VSTOP");
                racunski.racunaj_pozornost_in_utrujenost();
            }, t1);
        }

        function ugasniCasovnik() {
            console.log("Ugasam casovnik");
            clearCorrectingInterval(spremenljivkaCasovnik);
        }

        return {
            zacniCasovnik: zacniCasovnik,
            ugasniCasovnik: ugasniCasovnik
        };

    }]);

})();
