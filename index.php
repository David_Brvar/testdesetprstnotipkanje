<?php

session_start();

if(!isset($_SESSION['LogedIn']) || $_SESSION['LogedIn'] == "false")
{
	header("Location: login.php");
}

?>

<!doctype html>
<html class="no-js" lang="sl" ng-app="mojApp">
<!-- https://github.com/h5bp/html5-boilerplate/blob/5.2.0/dist/doc/html.md -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Desetprstno tipkanje</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<!-- Place favicon.ico in the root directory -->

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">

	<script src="js/vendor/modernizr-2.8.3.min.js"></script>
	<!-- BOOTSTRAP iz http://getbootstrap.com/2.3.2/ downloadan na disk -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
</head>
<!-- nad povrsino aplikacije v FF in Chrome:
- grabanje teksta ni mogoce
- cursor je povsod DEFAULT, razen nad linki pointer
- desni klik onemogocen -->
<body style="cursor: default;" class="neDaSeIzbratiTeksta" oncontextmenu="return false;">
	<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	<div id="vsebina" ui-view></div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
	<script src="js/plugins.js"></script>
	<script src="js/main.js"></script>

	<!-- najnovejsa stable -->
	<script src="js/vendor/angular.min.js"></script>
	<!--<script src="js/vendor/angular-route.min.js"></script>-->
	<script src="js/vendor/angular-ui-router.min.js"></script>
	<script src="js/vendor/ui-bootstrap-tpls-0.13.0.min.js"></script>

	<script src="js/app.js"></script>
	<script src="js/controllers.js"></script>
	<script src="js/services.js"></script>
	<script src="js/directives.js"></script>
	<script src="js/canvasStuff.js"></script>
	<script src="js/racunskiModel.js"></script>

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
	<script>
	/*(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
	function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
	e=o.createElement(i);r=o.getElementsByTagName(i)[0];
	e.src='https://www.google-analytics.com/analytics.js';
	r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
	ga('create','UA-XXXXX-X','auto');ga('send','pageview');*/
	</script>

</body>
</html>
