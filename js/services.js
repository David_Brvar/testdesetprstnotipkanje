(function() {
	var app = angular.module("mojServices", []);
	app.service("sharedStates", function() {

		var besedila;
		var selectedLevel = 0;
		//var difficulty = 0;

		var rezultat = {
			steviloPravilnih: 0,
			steviloNapacnih: 0
		};

		var userText = "";
		var napacne = [];

		var numberOfRight = 0,
			numberOfLeft = 0;

		return {
			dobiIgranoStopnjo: function() {
				return selectedLevel;
			},
			spremeniIgranoStopnjo: function(x) {
				selectedLevel = x;
			},
			/* dobi besedila vaj za tipkanje */
			saveExercisesText: function(x) {
				besedila = x;
			},
			setUserText: function(x) {
				userText = x;
				console.log("HAHAH usertext je bolan", userText);
			},
			getUserText: function() {
				return userText;
			},
			setUserMistakes: function(x) {
				napacne = x;
			},
			getUserMistakes: function() {
				return napacne;
			},
			posljiRezultat: function (a, b) {
				rezultat.steviloPravilnih = a;
				rezultat.steviloNapacnih = b;
			},
			dobiRezultat: function() {
				return rezultat;
			},

			getExerciseText: function() {
				//return (besedila[difficulty].vaje_left.join(" ") + besedila[difficulty].vaje_right.join(" "));
				return besedila[selectedLevel].vaje_left[0];
			}/*,*/

			/*getTextLevels: function() {
				return besedila;
			},*/
			/*setDifficulty: function(value) {
				difficulty = value;
			}/*,
			changeDifficulty: function(value) {
				value ? difficulty++ : difficulty--;
				if(difficulty < 0) {
					difficulty = 0;
				}
				if(difficulty > 14) {
					difficulty = 14;
				}
			}*/
			/*getSound: function() {
				return sound;
			},
			toogleSound: function() {
				sound ? sound = false : sound = true;
			}*/
		};
	});










	app.service("storitveREST", ["$http", function($http) {

		var promiseUserUnlockedLevelsAndAchievements,
			promiseTestData,
			promiseUnlockLevel,
			promiseExercisesText,
			promiseAuthentication;

		// zastavice ki preprecijo ponovno nalaganje stvari, ki so ze v brskalnikovem pomnilniku
		var flagAuthentication = true,
			flagExercisesText = true;

			/*$http({method: 'POST', url: 'php/beri_stopnje.php', headers: {'Content-Type': 'application/json'}})
			.success(function(data, status, headers, config) {
				//The API call to the back-end was successful (i.e. a valid session)
				console.log("Sem v funkciji beriStopnje .");
				console.log(data);

				for (var i = 0; i < data.length; i++) {
					self.dosezkiUporabnika[parseInt(data[i]['id_levels']) - 1] = parseInt(data[i]['max(achievements.id_achievements)']);
				}

				console.log(self.dosezkiUporabnika);
			})
			.error(function(data, status, headers, config) {
				console.log("Napaka v funkciji beriStopnje .");*/


		// vse spodaj so ASYNCANI REQUESTI
		return {
			postUnlockLevel: function(x) {
				console.log("Loading postUnlockLevel");
				var poslanObjekt = x;
				promiseUnlockLevel = $http({method: "POST", url: "php/odkleni_stopnjo.php", data: poslanObjekt});
				return promiseUnlockLevel;
			},
			postTestData: function(x) {
				console.log("Loading postTestData");
				var poslanObjekt = {
					tipke: x
				};
				promiseTestData = $http({method: "POST", url: "php/vpisi_v_bazo_test.php", data: poslanObjekt});
				return promiseTestData;
			},
			requestUserUnlockedLevelsAndAchievements: function() {
				console.log("Loading requestUserUnlockedLevelsAndAchievements");
				promiseUserUnlockedLevelsAndAchievements = $http({method: 'POST', url: 'php/beri_stopnje.php'});
				return promiseUserUnlockedLevelsAndAchievements;
			},
			requestExercisesText: function() {
				if (flagExercisesText) {
					console.log("Loading promiseExercisesText - flag is true");
					promiseExercisesText = $http({method: 'POST', url: 'vaje.json'});
					flagExercisesText = false;
					return promiseExercisesText;
				} else {
					console.log("Loading promiseExercisesText - flag is false");
					return promiseExercisesText;
				}
			},
			requestAuthentication: function() {
				if (flagAuthentication) {
					console.log("Loading requestAuthentication - flag is true");
					promiseAuthentication = $http({method: 'POST', url: 'php/avtentikacija.php'});
					flagAuthentication = false;
					return promiseAuthentication;
				} else {
					console.log("Loading requestAuthentication - flag is false");
					return promiseAuthentication;
				}
			}
		};

	}]);









	app.factory('audioPlayback', function() {

		var zvokVklopljen = true,
			path = "sounds/Chris-zabriskie-nirvana-relaxing-piano-music.mp3",
			jakost = 0.5;

		var music = new Audio(path);
		music.volume = jakost;
		music.loop = true; // se ponavlja

		var toogleSound = function() {
			zvokVklopljen ? zvokVklopljen = false : zvokVklopljen = true;
			console.log("V toogle sound.");
			menuMusic();
		};

		var getSound = function() {
			return zvokVklopljen;
		};

		var menuMusic = function() {
			if (zvokVklopljen) {
				//* odkomentiraj spodaj za glasbo v menu */
				music.play();
				console.log("Predvajam background glasbo.");
			} else {
				// firefox bug ob vrednosti zvokVklopljen = false na začetku !!!
				music.pause();
				music.currentTime = 0; // ce se zvok ugasne in spet prizge, muzika ponovno starta od zacetka
			}
		};

		var soundEffect = function(path, soundLevel) {
			// ce zvok omogocen
			if (zvokVklopljen) {
				// buffers automatically when created
				this.sound = new Audio(path);
				this.sound.volume = soundLevel;
				this.sound.play();
			}

			// za glasbo menuja timeout tece vseskozi v ozadju!
			/*if(path === "sounds/Ambiphonic-lounge-easy-listening-music.mp3") {
				setTimeout(function() {
					playAudio("sounds/Ambiphonic-lounge-easy-listening-music.mp3", 0.5);
				}, 300000);
			};*/
		};

		console.log("Sem v factory audioPlayback .");
		menuMusic();

		return {
			playSound: soundEffect,
			menuMusic: menuMusic,
			toogleSound: toogleSound,
			getSound: getSound
		};

	});

})();
