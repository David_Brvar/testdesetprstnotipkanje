<?php

	session_start();

	if (!(isset($_SESSION['LogedIn']) && $_SESSION['LogedIn'] == true))
	{
	header("Location: php/login.php");
	}
	else
	{
		require_once '../include/config.php';
		$input = json_decode(file_get_contents('php://input'));

		// logira nazaj
		echo "Vpisal bos cas: " . $input->cas . ". Ti si uporabnik " . $_SESSION['UserName'] . "<br />";

		/* logganje vardump v bazo
		ob_start();
		var_dump($user);
		$result = ob_get_clean();*/

		/* PDO nacin */
		try
		{
			$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE, DB_USER, DB_PASSWORD);
			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// nacin z prepared statements
			$sql = $conn->prepare("INSERT INTO results (time_spend, komentar) VALUES (:time_spend, :komentar)");
			$sql->bindParam(':time_spend', $input->cas);
			$sql->bindParam(':komentar', $input->komentar);
			$sql->execute();
			echo "New record created successfully";
		}
		catch(PDOException $e)
		{
			echo $sql . "<br />" . $e->getMessage();
		}

		$conn = null;

	}

?>
