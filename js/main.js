// sprememba med 100 in 300, 200 stopenj nasmeha/zalosti
function smejko(sprememba) {
	if (sprememba) {
		console.log("RISANJE SMEJKA ob spremembi", sprememba.pozornost);
		var sprememba = 300 - sprememba.pozornost;
		if(sprememba < 100) {
			sprememba = 100;
		}
		console.log("RISANJE SMEJKA z spremembo (300 - sprememba) z najmanjšo možno 100", sprememba);
		if(document.getElementById('smejko')) {
			var canvas = document.getElementById('smejko');
			var context = canvas.getContext('2d');
			var centerX = canvas.width / 2;
			var centerY = canvas.height / 2;
			var radius = canvas.width/2 - 5;
			var eyeRadius = 20;
			var eyeXOffset = 45;

			// draw the yellow circle
			context.beginPath();
			context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);

			var grd=context.createRadialGradient(centerX,centerY,radius,5,5,5);
			grd.addColorStop(0,"#D4EB57");
			grd.addColorStop(1,"white");

			context.fillStyle = grd;

			context.fill();
			context.lineWidth = 3;
			context.strokeStyle = 'black';
			context.stroke();

			// draw the eyes
			context.beginPath();
			var eyeX = centerX - eyeXOffset;
			var eyeY = centerY - eyeXOffset;
			context.arc(eyeX, eyeY, eyeRadius, 0, 2 * Math.PI, false);
			var eyeX = centerX + eyeXOffset;
			context.arc(eyeX, eyeY, eyeRadius, 0, 2 * Math.PI, false);
			context.fillStyle = 'black';
			context.fill();

			// usta
			context.beginPath();

			context.moveTo(150-75, 150+25);
			context.quadraticCurveTo(150, sprememba, 150+75, 150+25);
			context.stroke();
		}
	} else {
		var sprememba = 175;
		var canvas = document.getElementById('smejko');
		var context = canvas.getContext('2d');
		var centerX = canvas.width / 2;
		var centerY = canvas.height / 2;
		var radius = canvas.width/2 - 5;
		var eyeRadius = 20;
		var eyeXOffset = 45;

		// draw the yellow circle
		context.beginPath();
		context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);

		var grd=context.createRadialGradient(centerX,centerY,radius,5,5,5);
		grd.addColorStop(0,"#D4EB57");
		grd.addColorStop(1,"white");

		context.fillStyle = grd;

		context.fill();
		context.lineWidth = 3;
		context.strokeStyle = 'black';
		context.stroke();

		// draw the eyes
		context.beginPath();
		var eyeX = centerX - eyeXOffset;
		var eyeY = centerY - eyeXOffset;
		context.arc(eyeX, eyeY, eyeRadius, 0, 2 * Math.PI, false);
		var eyeX = centerX + eyeXOffset;
		context.arc(eyeX, eyeY, eyeRadius, 0, 2 * Math.PI, false);
		context.fillStyle = 'black';
		context.fill();

		// usta
		context.beginPath();

		context.moveTo(150-75, 150+25);
		context.quadraticCurveTo(150, sprememba, 150+75, 150+25);
		context.stroke();
	}
}

var updateProgress = function(value) {
	document.getElementById('progress-bar-moving').style.width = "" + value + "%";
}
