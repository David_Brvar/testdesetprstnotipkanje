<?php

	session_start();

	if (!(isset($_SESSION['LogedIn']) && $_SESSION['LogedIn'] == true))
	{
		header("Location: php/login.php");
	}
	else
	{
		require_once '../include/config.php';
		/* PDO nacin */
		try
		{
			$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE, DB_USER, DB_PASSWORD);
			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// nacin z prepared statements
			$sql1 = $conn->prepare("SELECT count(*) from levels;");
			$sql1->execute();

			$sql2 = $conn->prepare("SELECT count(id_levels) from levels;");
			$sql2->execute();

			// set the resulting array to associative
			$results = array();
			//$results[] = $sql1->fetchAll(PDO::FETCH_ASSOC);
			//$results[] = $sql2->fetchAll(PDO::FETCH_ASSOC);
			$results[] = $sql1->fetch(PDO::FETCH_ASSOC);
			$results[] = $sql2->fetch(PDO::FETCH_ASSOC);
			echo json_encode($results);
		}

		catch(PDOException $e)
		{
			echo $sql . "<br />" . $e->getMessage();
		}

		$conn = null;
	}

?>
