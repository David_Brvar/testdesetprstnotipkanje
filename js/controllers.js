(function() {

	var app = angular.module("mojControllers", []);
	app.controller("TipkanjeController", ["$document", "$scope", "$state", "sharedStates", "storitveREST", "audioPlayback", "canstuff", "racunski", "time", function($document, $scope, $state, sharedStates, storitveREST, audioPlayback, canstuff, racunski, time) {

		console.log("Zagnan controller za tipkanje .");

		/* INICIALIZACIJA */
		racunski.prazniMeritve();
		canstuff.inicializirajCanvas();
		var self = this; // brez rabe angular.bind

		/* glede na tezavnost servira nek tekst */
		self.dbText = sharedStates.getExerciseText();
		self.userText = ""; // uft8 za blank space je \u00A0

		sharedStates.setUserText(self.userText);
		self.steviloPravilnihPritiskov = 0;

		self.napacne = [];
		sharedStates.setUserMistakes(self.napacne);
		self.steviloNapacnihPritiskov = 0;

		smejko(undefined);

		//---------------prikaz

		self._pravilnosti = [];
		self._pozornosti = [];
		self._utrujenosti = [];

		//---------------------

		time.zacniCasovnik();

		var casovnik_1 = new Date().getTime(),
			casovnik_2 = new Date().getTime();

		$document.unbind("keydown.TipkanjeController"); // ensure it will bind once
		$document.bind("keydown.TipkanjeController", function(e) {
			/* cas udarca po tipki je unix timestamp */
			casovnik_2 = new Date().getTime();
			izvedenKeydown(e);
		});

		function izvedenKeydown(event) {
			console.log("Sem v funkciji izvedenKeydown .");
			// chrome vs firefox
			var x = event.which || event.keyCode;

			if (x == 8) { /* prepreci BACKSPACE */
				event.preventDefault();
			} else if (112 <= x && x <= 123) { /* prepreci F1 - F12 */
				event.preventDefault();
			} else if (x == 32) { /* SPACE */
				event.preventDefault();
				dodajCrko(event);
			}
		}

		$document.unbind("keypress.TipkanjeController"); // ensure it will bind once
		$document.on("keypress.TipkanjeController", function(e) {
			izvedenKeypress(e);
		});

		function izvedenKeypress(event) {
			console.log("Sem v funkciji izvedenKeypress .");
			dodajCrko(event);
		}

		function dodajCrko(e) {
			console.log("Sem v funkciji dodajCrko .");

			var x = e.which || e.keyCode,
				uporabnikovaCrka = String.fromCharCode(x);
			// razlikuje velike in male crke na podlagi pritiska na SHIFT
			if (e.shiftKey == false) {
				uporabnikovaCrka = uporabnikovaCrka.toLowerCase();
			}

			var pravilnaCrka = self.dbText[self.userText.length];
			self.userText += pravilnaCrka;
			sharedStates.setUserText(self.userText);

			// racunaj pravilnost na podlagi zakasnitve, pravilne crke in uporabnikove crke
			racunski.racunajPravilnost(Math.abs(casovnik_2 - casovnik_1), pravilnaCrka, uporabnikovaCrka);

			// PRAVILEN STISK TIPKE --------------------------------------------------
			if(pravilnaCrka === uporabnikovaCrka) {
				self.steviloPravilnihPritiskov++;
				self.napacne.push(0);
				sharedStates.setUserMistakes(self.napacne);

				audioPlayback.playSound("sounds/correctLetter.mp3", 0.1);

				canstuff.spremeniBarvoOzadja("posvetli");
			} else {
				// NAPACEN STISK TIPKE ---------------------------------------------------
				self.steviloNapacnihPritiskov++;
				self.napacne.push(1);
				sharedStates.setUserMistakes(self.napacne);

				canstuff.spremeniBarvoOzadja("potemni");
			}
			$scope.$apply();
		}

		/* angularjs watch funkcija */

		$scope.$watch(function() {
				return self.userText;
			}, function (prej, potem) {
			// console.log("zagnan watch");
			// console.log("--------------------------------");
			canstuff.risiCrke();

			self._pravilnosti = racunski.dobiPravilnosti();
			self._pozornosti = racunski.dobiPozornosti();
			self._utrujenosti = racunski.dobiUtrujenosti();

			// posreduj zadnjo pozornost za risanje smejkota -------------------
			// smejko(self._pozornosti[self._pozornosti.length - 1]);

			// izrisujGraf(self._pravilnosti, self._pozornosti, self._utrujenosti);

			// console.log("Zakasnitev je bila ", casovnik_2 - casovnik_1);

			/* CAS POJAVITVE NOVE CRKE */
			casovnik_1 = new Date().getTime();

			checkVictory();
		});

		function checkVictory() {
			if (self.userText.length === self.dbText.length) {
				time.ugasniCasovnik();
				$state.go("statistika");
				console.log("Klical bom praznjenje.");

				sharedStates.posljiRezultat(self.steviloPravilnihPritiskov, self.steviloNapacnihPritiskov);

			}

			// klic testnega vpisa v bazo
			// self.vpisiVBazoTest();
			storitveREST.postTestData(self.userText)
				.success(function(data, status, headers, config) {
					//The API call to the back-end was successful (i.e. a valid session)
					console.log("Sem v funkciji storitveREST.postTestData .");
				})
				.error(function(data, status, headers, config) {
					console.log("Napaka v funkciji storitveREST.postTestData .");
				});

		}

		/*self.vpisiVBazoTest = function() {

			var poslanObjekt = {
				tipke: self.userText
			};
			$http({method: "POST", url: "php/vpisi_v_bazo_test.php", headers: {"Content-Type": "application/json"}, data: poslanObjekt});
		};*/


		/* NACIN Z PRIPENJANJEM OZ BINDINJAM THIS IZ GLOBALNEGA SCOPA V FUNKCIJO
		var checkVictory = angular.bind(this, function() {
			if(this.userText.length === this.dbText.length) {
				time.ugasniCasovnik();
				$state.go("statistika");
				console.log("Klical bom praznjenje.");

				sharedStates.posljiRezultat(self.steviloPravilnihPritiskov, self.steviloNapacnihPritiskov);
			}
		}); */

		/* this.unici = function() {
			console.log("Unicen controller od tipkanja.");
			$state.reload("menu");
		}; */

	}]);

	app.controller("StatistikaController", ["sharedStates", "storitveREST", "$state", "racunski", function(sharedStates, storitveREST, $state, racunski) {

		console.log("Zagnan controller za statistika .");
		var self = this;

		racunski.oddajMeritve();

		self.rezultat = sharedStates.dobiRezultat();

		self.izracun = 100 * self.rezultat.steviloPravilnih / (self.rezultat.steviloPravilnih + self.rezultat.steviloNapacnih);
		self.zvezdice = 0;

		var unlock = false;

		setTimeout(animiraj, 500);
		function animiraj() {
			var x = document.getElementsByClassName("fave");
			console.log("Zacenjam animacijo zvezdic.");

			x[0].className = "fave ongoing";
			setTimeout(function() {
				x[1].className = "fave ongoing";
			}, 250);
			setTimeout(function() {
				x[2].className = "fave ongoing";
			}, 500);
		}

		self.zvezdice = 0;
		if (self.izracun > 90) {
			self.zvezdice = 4;
			unlock = true;
		} else if (self.izracun > 65) {
			self.zvezdice = 3;
			unlock = true;
		} else if (self.izracun > 50) {
			self.zvezdice = 2;
		} else {
			self.zvezdice = 1;
		}

		var poslanObjekt = {
			trenutnaStopnja: sharedStates.dobiIgranoStopnjo(),
			rezultatTrenutneStopnje: self.zvezdice,
			odklepNaslednjeStopnje: unlock,
			komentar : "odklepam stopnje like a boss"
		};
		/*$http({method: "POST", url: "php/odkleni_stopnjo.php", headers: {"Content-Type": "application/json"}, data: poslanObjekt});*/

		storitveREST.postUnlockLevel(poslanObjekt)
			.success(function(data, status, headers, config) {
				//The API call to the back-end was successful (i.e. a valid session)
				console.log("Sem v funkciji storitveREST.postUnlockLevel .");
			})
			.error(function(data, status, headers, config) {
				console.log("Napaka v funkciji storitveREST.postUnlockLevel .");
			});

	}]);

	app.controller("ProfileController", ["$scope", "sharedStates", "$modal", function($scope, sharedStates, $modal) {

		var self = this;
		console.log("Zagnan controller za profile .");

		self.x = "ControllerAs tekst => Screen userjevega profila.";

		$scope.name = "$scope tekst => Pred odprtjem modal okna.";

		self.open = function() {
			self.$modalInstance = $modal.open({
				scope: $scope,
				templateUrl: "partials/modal.html",
				size: "",
				backdrop: false
			})
		};

		self.ok = function() {
			self.$modalInstance.close();
			$scope.name = "Modal okno se odprlo, user pritisnil DA.";
		};

		self.cancel = function() {
			self.$modalInstance.dismiss("cancel");
			$scope.name = "Modal okno se odprlo, user pritisnil NE.";
		};

	}]);

	app.controller("MenuController", ["sharedStates", "storitveREST", "audioPlayback", "$state", function(sharedStates, storitveREST, audioPlayback, $state) {

		var self = this;
		console.log("Zagnan controller za menu .");
		self.userAchievements = [];
		self.navodila = [];

		// HARDKODIRANO STEVILO STOPENJ (15)
		for (var i = 0; i < 15; i++) {
			self.userAchievements.push(0);
		}

		self.dobiStopnjo = function() {
			return sharedStates.dobiIgranoStopnjo();
		};

		self.setDifficulty = function($index) {

			sharedStates.spremeniIgranoStopnjo($index);
			self.navodilo = self.navodila[sharedStates.dobiIgranoStopnjo()].navodilo;
			sharedStates.setDifficulty($index);

			/*var poslanObjekt = {
				cas : $index,
				komentar : "random komentar"
			};
			$http({method: "POST", url: "php/vpisi_v_bazo.php", headers: {"Content-Type": "application/json"}, data: poslanObjekt});*/
		};

		/*self.beriStopnje = function(value) {
			$http({method: 'POST', url: 'php/beri_stopnje.php', headers: {'Content-Type': 'application/json'}})
			.success(function(data, status, headers, config) {
				//The API call to the back-end was successful (i.e. a valid session)
				console.log("Sem v funkciji beriStopnje .");
				console.log(data);

				for (var i = 0; i < data.length; i++) {
					self.userAchievements[parseInt(data[i]['id_levels']) - 1] = parseInt(data[i]['max(achievements.id_achievements)']);
				}

				console.log(self.userAchievements);
			})
			.error(function(data, status, headers, config) {
				console.log("Napaka v funkciji beriStopnje .");
			});
		};*/
		//self.beriStopnje();

		/*self.beriNavodila = function(value) {
			$http({method: "POST", url: "php/beri_navodila.php", headers: {"Content-Type": "application/json"}})
			.success(function(data, status, headers, config) {
				//The API call to the back-end was successful (i.e. a valid session)
				console.log("Sem v funkciji beriNavodila .");
				console.log(data);
			})
			.error(function(data, status, headers, config) {
				console.log("Napaka v funkciji beriNavodila .");
			});
		};
		self.beriNavodila();*/

		storitveREST.requestUserUnlockedLevelsAndAchievements()
			.success(function(data, status, headers, config) {
				//The API call to the back-end was successful (i.e. a valid session)
				console.log("Sem v funkciji storitveREST.requestUserUnlockedLevelsAndAchievements .");
				console.log(data);

				for (var i = 0; i < data.length; i++) {
					self.userAchievements[parseInt(data[i]['id_levels']) - 1] = parseInt(data[i]['max(achievements.id_achievements)']);
				}

				console.log(self.userAchievements);
			})
			.error(function(data, status, headers, config) {
				console.log("Napaka v funkciji beriStopnje .");
			});


		//self.steviloStopenj;
		storitveREST.requestExercisesText()
			.success(function(data, status, headers, config) {
				//The API call to the back-end was successful (i.e. a valid session)
				console.log("Sem v funkciji storitveREST.requestExercisesText .");
				//console.table(data);
				self.navodila = data;
				//self.steviloStopenj = data.length;

				self.navodilo = self.navodila[sharedStates.dobiIgranoStopnjo()].navodilo;
				sharedStates.saveExercisesText(self.navodila);
			})
			.error(function(data, status, headers, config) {
				console.log("Napaka v funkciji storitveREST.requestExercisesText .");
			});


		self.user = "";
		storitveREST.requestAuthentication()
			.success(function(data, status, headers, config) {
				//The API call to the back-end was successful (i.e. a valid session)
				console.log("Sem v funkciji storitveREST.requestAuthentication .");
				self.user = data;
			})
			.error(function(data, status, headers, config) {
				console.log("Napaka v funkciji storitveREST.requestAuthentication .");
			});


		self.toogleSound = function() {
			audioPlayback.toogleSound();
		};
		self.getSound = function() {
			return audioPlayback.getSound();
		};

		self.changeStateToTyping = function() {
			if (self.userAchievements[sharedStates.dobiIgranoStopnjo()] > 0) {
				$state.go("tipkanje");
			} else {
				alert("Nimas dovoljenj za to stopnjo!");
			}
		};

	}]);

})();
