(function() {

	var app = angular.module("mojApp", ["ui.router", "mojControllers", "mojServices", "mojRacunskiModel", "mojCanvasStuff", "ui.bootstrap"]);
	app.config(["$urlRouterProvider", "$stateProvider", function($urlRouterProvider, $stateProvider) {

		// For any unmatched url, redirect
		$urlRouterProvider
			.otherwise("/");

		$stateProvider
			.state("menu", {
				url: "/",
				templateUrl: "partials/menu.html",
				controller: "MenuController",
				controllerAs: "MenuCtrl"
			})
			.state("profile", {
				url: "/profile",
				templateUrl: "partials/profile.html",
				controller: "ProfileController",
				controllerAs: "ProfileCtrl"
			})
			.state("tipkanje", {
				url: "/tipkanje",
				templateUrl: "partials/tipkanje.html",
				controller: "TipkanjeController",
				controllerAs: "TipkanjeCtrl"
			})
			.state("statistika", {
				// namerno brez urlja
				templateUrl: "partials/statistika.html",
				controller: "StatistikaController",
				controllerAs: "StatistikaCtrl"
			})

	}]);

	app.run(["$location", function($location) {
		// ob reloadu se aplikacija vedno postavi na zacetek
		$location.path("/");
	}]);

})();
