<?php

	session_start();

	if (!(isset($_SESSION['LogedIn']) && $_SESSION['LogedIn'] == true))
	{
		header("Location: php/login.php");
	}
	else
	{
		require_once '../include/config.php';

		/* PDO nacin */
		try
		{
			$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE, DB_USER, DB_PASSWORD);
			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// nacin z prepared statements
			$sql = $conn->prepare("SELECT users.username, levels.id_levels, max(achievements.id_achievements) FROM users_2_levels_2_achievements JOIN users on users_2_levels_2_achievements.users_fk = users.unique_id JOIN levels on users_2_levels_2_achievements.levels_fk = levels.id_levels JOIN achievements on users_2_levels_2_achievements.achievements_fk = achievements.id_achievements WHERE users.username ='".$_SESSION['UserName']."' GROUP BY users.username, levels.id_levels");
			$sql->execute();

			// set the resulting array to associative
			$results = $sql->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode($results);
		}

		catch(PDOException $e)
		{
			echo $sql . "<br />" . $e->getMessage();
		}

		$conn = null;
	}

?>
